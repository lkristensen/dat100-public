package no.hib.dat100.stabel;

public class MinUncheckedException extends RuntimeException {

	public MinUncheckedException(String msg) {
		super(msg);
	}

}
