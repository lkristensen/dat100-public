package no.hib.dat100.mybrowser;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

import javax.swing.JTextArea;
import javax.swing.JTextField;

public class GoBtnListener implements ActionListener {

	private JTextArea webpagetextarea;
	private JTextField urltextfield;

	public GoBtnListener(JTextArea webpagetextarea, JTextField urltextfield) {
		this.webpagetextarea = webpagetextarea;
		this.urltextfield = urltextfield;
	}

	public void actionPerformed(ActionEvent e) {

		String urlstr = urltextfield.getText();
		String text = "";

		// TODO - START
		
		// URL url = new URL(urlstr);
			
		// Scanner in = new Scanner(url.openStream());
			
		// accumulate lines received via the URL in text
		// read max MyBrowser.MAX_LINES
		// use a while loop and hasNextLine method on in
			

	    // put the text into the web text area
		webpagetextarea.setText("Go button pressed with URL = " + urlstr);
			
		// close the Scanner;
			
		// catch any exceptions;
			
		// always clear the url text field after each "Go"
		urltextfield.setText("");
		
		// TODO - SLUTT
	}
}
