package no.hib.dat100.kontakter;


public class KontaktListeLagringTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		KontakListeMedLagring kl = new KontakListeMedLagring();
		
		kl.leggtilNummer("Lars",KontaktType.MOBIL,93866491);
		kl.leggtilNummer("Sven-Olai",KontaktType.KONTOR,87563);
		
		KontaktInfo kilmk = kl.finnKontaktInfo("Lars");
		System.out.println("Lars");
		kilmk.visDetaljer();
		
		KontaktInfo kisoh = kl.finnKontaktInfo("Sven-Olai");
		System.out.println("Sven-Olai");
		kisoh.visDetaljer();
		
		kl.skrivTilFil("kontaktliste.txt");
		
	}

}
